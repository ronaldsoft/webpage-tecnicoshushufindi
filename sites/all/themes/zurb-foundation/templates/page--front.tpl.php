<!--.page -->
<div role="document" class="page">

  <!--.l-header -->
  <header role="banner" class="l-header">
    <?php if ($top_bar): ?>
      <!--.top-bar -->      

      <?php if ($top_bar_classes): ?>
        <div class="<?php print $top_bar_classes; ?>">
      <?php endif; ?>
     <nav class="top-bar"<?php print $top_bar_options; ?>>
          <ul class="title-area">
          <li class="name"><h1><?php print $linked_site_name; ?></h1></li>
          <li class="toggle-topbar menu-icon"><a href="#"><span><?php print $top_bar_menu_text; ?></span></a></li>
        </ul>
        <section class="top-bar-section">         
          <?php if ($top_bar_main_menu) :?>
            <?php print $top_bar_main_menu; ?>
          <?php endif; ?>
          <?php if ($top_bar_secondary_menu) :?>
            <?php print $top_bar_secondary_menu; ?>
          <?php endif; ?>        
           <ul class="right" >
            <li class="search">                            
                 <?php print $search_box; ?>                     
            </li>            
          </ul>   
        </section>         
            </nav>
      <?php if ($top_bar_classes): ?>              
        </div>        
      <?php endif; ?>      
      <!--/.top-bar -->
       <?php endif; ?>
    <!-- Title, slogan and menu -->
    <?php if ($alt_header): ?>
      <section class="row <?php print $alt_header_classes; ?>">

        <?php if ($linked_logo): print $linked_logo; endif; ?>

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name" class="element-invisible">
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <h2 title="<?php print $site_slogan; ?>" class="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>

        <?php if ($alt_main_menu): ?>
          <nav id="main-menu" class="navigation" role="navigation">
            <?php print ($alt_main_menu); ?>
          </nav> <!-- /#main-menu -->
        <?php endif; ?>
        <?php if ($alt_secondary_menu): ?>
          <nav id="secondary-menu" class="navigation" role="navigation">
            <?php print $alt_secondary_menu; ?>

          </nav> <!-- /#secondary-menu -->
        <?php endif; ?>
      </section>
    <?php endif; ?>
    <!-- End title, slogan and menu -->
    <?php print render($page['search']); ?>
      <div class="row">
    <div class="large-12 columns">
      <div class="row">
        <div class="large-3 small-6 columns">
          <img src="http://tecnicoshushufindi.com/sites/all/themes/zurb-foundation/images/clambake.jpg"/>
          <h6 class="panel">Description</h6>
        </div> 
        <div class="large-3 small-6 columns">
          <img src="http://tecnicoshushufindi.com/sites/all/themes/zurb-foundation/images/off-canvas-layouts.jpg"/>
          <h6 class="panel">Description</h6>
        </div> 
        <div class="large-3 small-6 columns">
          <img src="http://tecnicoshushufindi.com/sites/all/themes/zurb-foundation/images/thumb.jpg"/>
          <h6 class="panel">Description</h6>
        </div> 
        <div class="large-3 small-6 columns">
          <img src="http://tecnicoshushufindi.com/sites/all/themes/zurb-foundation/images/thumbnail.jpg"/>
          <h6 class="panel">Description</h6>
        </div>
 
      <div class="row">
              <div class="large-6 small-6 columns">
                <div class="panel">
                  <h5>Header</h5>
                <h6 class="subheader">Praesent placerat dui tincidunt elit suscipit sed.</h6>
                <a href="#" class="small button">BUTTON TIME!</a>
                </div>
              </div>
 
              <div class="large-6 small-6 columns">
                <div class="panel">
                  <h5>Header</h5>
                <h6 class="subheader">Praesent placerat dui tincidunt elit suscipit sed.</h6>
                <a href="#" class="small button">BUTTON TIME!</a>
                </div>
              </div>
          </div>
 <div class="row">
              <div class="large-6 small-6 columns">
                <div class="panel">
                  <h5>Header</h5>
                <h6 class="subheader">Praesent placerat dui tincidunt elit suscipit sed.</h6>
                <a href="#" class="small button">BUTTON TIME!</a>
                </div>
              </div>
 
              <div class="large-6 small-6 columns">
                <div class="panel">
                  <h5>Header</h5>
                <h6 class="subheader">Praesent placerat dui tincidunt elit suscipit sed.</h6>
                <a href="#" class="small button">BUTTON TIME!</a>
                </div>
              </div>
          </div>

<div class="row">
  
    <div class="large-8 columns">
      <h4>This is a content section.</h4>
      <p>Bacon ipsum dolor sit amet nulla ham qui sint exercitation eiusmod commodo, chuck duis velit. Aute in reprehenderit, dolore aliqua non est magna in labore pig pork biltong. Eiusmod swine spare ribs reprehenderit culpa. Boudin aliqua adipisicing rump corned beef.</p>
      <p><a href="#" class="secondary small button">Next Page →</a></p>
    </div>

           <div class="large-4 columns">
    
       
      
      <ul class="small-block-grid-3">
        <li><a href="#"><img src="http://placehold.it/120x120" style="border-radius: 100px;"/></a></li>
        <li><a href="#"><img src="http://placehold.it/120x120" style="border-radius: 100px;"/></a></li>
        <li><a href="#"><img src="http://placehold.it/120x120" style="border-radius: 100px;"/></a></li>
        <li><a href="#"><img src="http://placehold.it/120x120" style="border-radius: 100px;"/></a></li>
        <li><a href="#"><img src="http://placehold.it/120x120" style="border-radius: 100px;"/></a></li>
        <li><a href="#"><img src="http://placehold.it/120x120" style="border-radius: 100px;"/></a></li>
      </ul>
    </div>
       
  </div>
  <div class="row">
 
    <div class="large-4 columns">
      <div class="panel">
        <p>
          <img src="http://placehold.it/400x300"/>
        </p>
      </div>
    </div>
    <div class="large-4 columns">
      <div class="panel">
        <p>
          <img src="http://placehold.it/400x300"/>
        </p>
      </div>
    </div>
    <div class="large-4 columns">
      <div class="panel">
        <p>
          <img src="http://placehold.it/400x300"/>
        </p>
      </div>
    </div>
 
  </div>

      </div>
            <section class="section">
          <h5 class="title"><a href="#panel2">Specific Person</a></h5>
          <div class="content" data-slug="panel2">
            <ul class="large-block-grid-5">
              <li><a href="mailto:mal@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Mal Reynolds</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:zoe@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Zoe Washburne</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:jayne@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Jayne Cobb</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:doc@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Simon Tam</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:killyouwithmymind@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">River Tam</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:leafonthewind@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Hoban Washburne</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:book@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Shepherd Book</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:klee@serenity.bc.reb"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Kaywinnet Lee Fry</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:inara@guild.comp.all"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Inarra Serra</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:inara@guild.comp.all"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Inarra Serra</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:inara@guild.comp.all"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Inarra Serra</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:inara@guild.comp.all"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Inarra Serra</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:inara@guild.comp.all"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Inarra Serra</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:inara@guild.comp.all"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Inarra Serra</a>
              <h6 class="subheader">Abanderado</h6></li>
              <li><a href="mailto:inara@guild.comp.all"><img src="http://placehold.it/200x200&text=[imageperson]" style="border-radius: 100px;">Inarra Serra</a>
              <h6 class="subheader">Abanderado</h6></li>
            </ul>
          </div>
        </section>
    </div>

  </div>

    <?php if (!empty($page['header'])): ?>
      <!--.l-header-region -->      
      <section class="l-header-region row">      
        <div class="large-12 columns">
          <?php print render($page['header']); ?>          
        </div>
      </section>
      <!--/.l-header-region -->
    <?php endif; ?>
      </header>
  <!--/.l-header -->
  <?php if (!empty($page['featured'])): ?>
    <!--.featured -->
    <section class="l-featured row">
      <div class="large-12 columns">
        <?php print render($page['featured']); ?>
      </div>
    </section>
    <!--/.l-featured -->
  <?php endif; ?>

  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <!--.l-messages -->
    <section class="l-messages row">
      <div class="large-12 columns">
        <?php if ($messages): print $messages; endif; ?>
      </div>
    </section>
    <!--/.l-messages -->
  <?php endif; ?>
  <?php if (!empty($page['help'])): ?>
    <!--.l-help -->
    <section class="l-help row">
      <div class="large-12 columns">
        <?php print render($page['help']); ?>
      </div>
    </section>
    <!--/.l-help -->
  <?php endif; ?>

  <main role="main" class="row l-main">
    <div class="<?php print $main_grid; ?> main columns">
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlight panel callout">
          <?php print render($page['highlighted']); ?>
        </div>
      <?php endif; ?>   

      <?php if ($breadcrumb): print $breadcrumb; endif; ?>

      <?php if ($title && !$is_front): ?>
        <?php print render($title_prefix); ?>
        <h1 id="page-title" class="title"><?php print $title; ?></h1>
        <?php print render($title_suffix); ?>
      <?php endif; ?>

      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
        <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
    </div>
    <!--/.l-main region -->
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside role="complementary" class="<?php print $sidebar_first_grid; ?> l-sidebar-first columns sidebar">
        <?php print render($page['sidebar_first']); ?>
      </aside>
    <?php endif; ?>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside role="complementary" class="<?php print $sidebar_sec_grid; ?> l-sidebar-second columns sidebar">
        <?php print render($page['sidebar_second']); ?>
      </aside>
    <?php endif; ?>
  </main>
  <!--/.l-main-->

  <?php if (!empty($page['footer_first']) || !empty($page['footer_middle']) || !empty($page['footer_last'])): ?>
    <!--.l-footer-->
    <footer class="l-footer panel row" role="contentinfo">
      <?php if (!empty($page['footer_first'])): ?>
        <div id="footer-first" class="large-4 columns">
          <?php print render($page['footer_first']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_middle'])): ?>
        <div id="footer-middle" class="large-4 columns">
          <?php print render($page['footer_middle']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_last'])): ?>
        <div id="footer-last" class="large-4 columns">
          <?php print render($page['footer_last']); ?>
        </div>
      <?php endif; ?>      
    </footer>
    <!--/.footer-->
  <?php endif; ?>

  <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->
